import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

const TechniciansList = () => {
  const [technicians, setTechnicians] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    const fetchTechnicians = async () => {
      try {
        const response = await fetch('http://localhost:8080/api/technicians/');
        const data = await response.json();
        setTechnicians(data.technicians);
      } catch (error) {
        console.log('Error fetching technicians:', error);
      }
    };

    fetchTechnicians();
  }, []);

  const handleCreateTechnician = async () => {
    try {
      const response = await fetch('http://localhost:8080/api/technicians/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          first_name: 'John',
          last_name: 'Doe',
          employee_id: '123456',
        }),
      });

      if (response.ok) {
        await response.json();
        navigate('/technicians');
      } else {
        throw new Error('Failed to create technician');
      }
    } catch (error) {
      console.log('Error creating technician:', error);
    }
  };

  return (
    <div>
      <h1>Technicians</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>
        <tbody>
          {technicians.map((technician) => (
            <tr key={technician.id}>
              <td>{technician.employee_id}</td>
              <td>{technician.first_name}</td>
              <td>{technician.last_name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default TechniciansList;
