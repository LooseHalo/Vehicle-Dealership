import React, { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturersList from './ManufacturersList';
import AutomobileList from './AutomobileList';
import ManufacturersForm from './ManufacturersForm';
import VehicleModelsList from './VehicleModelsList';
import VehicleModelForm from './VehicleModelForm';
import AutomobileForm from './AutomobileForm';
import SalesPeopleList from './SalesPeopleList';
import SalesPeopleForm from './SalesPeopleForm';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import TechnicianForm from './TechnicianForm';
import TechniciansList from './TechniciansList';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import AppointmentHistory from './AppointmentHistory';
import SalesList from './SalesList';
import SaleForm from './SaleForm';
import SalesPeopleHistory from './SalesPeopleHistory';


function App() {
  const [manufacturers, setManufacturers] = useState([]);
  const [automobiles, setAutomobiles] = useState([]);
  const [manufacturerName, setManufacturerName] = useState('');
  const [models, setModels] = useState([]);
  const [redirectToTechnicians, setRedirectToTechnicians] = useState(false);
  const [technicianData, setTechnicianData] = useState({
    firstName: '',
    lastName: '',
    employeeID: '',
  });
  const [technicians, setTechnicians] = useState([]);
  const [appointments, setAppointments] = useState([]);

  async function loadAutomobiles() {
    const response = await fetch('http://localhost:8100/api/automobiles/');
    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    }
  }

  async function loadManufacturers() {
    const response = await fetch('http://localhost:8100/api/manufacturers/');
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  }

  async function loadModels() {
    const response = await fetch('http://localhost:8100/api/models/');
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  }

  useEffect(() => {
    loadManufacturers();
    loadAutomobiles();
    loadModels();
    if (redirectToTechnicians) {
      window.location.href = '/technicians';
    }
    fetchTechnicians();
  }, [redirectToTechnicians]);

  const handleFormSubmit = async (event) => {
    event.preventDefault();
    try {
      const response = await fetch('http://localhost:8100/api/manufacturers/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ name: manufacturerName }),
      });
      if (response.ok) {
        setManufacturerName('');
        window.location.href = '/manufacturers';
      }
    } catch (error) {}
  };

  const handleAutomobileFormSubmit = async (automobileData) => {
    try {
      const response = await fetch('http://localhost:8100/api/automobiles/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(automobileData),
      });
      if (response.ok) {
        window.location.href = '/automobiles';
      }
    } catch (error) {}
  };

  const handleInputChange = (event) => {
    setManufacturerName(event.target.value);
  };

  const handleModelFormSubmit = async (newModel) => {
    try {
      const response = await fetch('http://localhost:8100/api/models/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newModel),
      });
    } catch (error) {}
  };

  const handleTechnicianFormSubmit = async (event) => {
    event.preventDefault();
    try {
      const response = await fetch('http://localhost:8080/api/technician/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(technicianData),
      });
      if (response.ok) {
        const data = await response.json();
        setTechnicianData({
          firstName: '',
          lastName: '',
          employeeID: '',
        });
        setRedirectToTechnicians(true);
      } else {
        console.error(response);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleTechnicianInputChange = (event) => {
    setTechnicianData({
      ...technicianData,
      [event.target.name]: event.target.value,
    });
  };

  const fetchTechnicians = async () => {
    try {
      const response = await fetch('/api/technician');
      const data = await response.json();
      setTechnicians(data.technicians);
    } catch (error) {
      console.log('Error fetching technicians:', error);
    }
  };


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/models" element={<VehicleModelsList models={models} />} />
          <Route
            path="/models/create"
            element={<VehicleModelForm onSubmit={handleModelFormSubmit} />}
          />
        <Route
          path="/manufacturers"
          element={<ManufacturersList manufacturers={manufacturers} />}
        />
        <Route
          path="/manufacturers/create"
          element={
            <ManufacturersForm
              name={manufacturerName}
              onChange={handleInputChange}
              onSubmit={handleFormSubmit}
            />
          }
        />
        <Route
          path="/technicians/create"
          element={
            redirectToTechnicians ? null : (
              <TechnicianForm
                formData={technicianData}
                onChange={handleTechnicianInputChange}
                onSubmit={handleTechnicianFormSubmit}
              />
            )
          }
        />
        <Route
          path="/technicians"
          element={<TechniciansList technicians={technicians} />} // Pass technicians prop
        />
        <Route
          path="/automobiles"
          element={<AutomobileList automobiles={automobiles} />}
        />
        <Route
          path="/automobiles/create"
          element={<AutomobileForm onSubmit={handleAutomobileFormSubmit} />}
        />
        <Route
          path="/salespeople"
          element={<SalesPeopleList />}
        />
        <Route
          path="/salespeople/create"
          element={<SalesPeopleForm />}
        />
        <Route
          path="/customers"
          element={<CustomerList />}
        />
        <Route path="/create-customer" element={<CustomerForm />} />
        <Route path="/appointments" element={<AppointmentList />} />
        <Route path="/appointments/create" element={<AppointmentForm />} />
        <Route path="/appointments/history" element={<AppointmentHistory appointments={appointments} />} />
      <Route
            path="/sales"
            element={<SalesList />}
          />
          <Route
            path="/sales/create"
            element={<SaleForm />}
          />
          <Route path="/salesperson-history" element={<SalesPeopleHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
