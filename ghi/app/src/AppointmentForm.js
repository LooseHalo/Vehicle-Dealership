import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function AppointmentForm() {
  const [date_time, setDate_time] = useState('');
  const [reason, setReason] = useState('');
  const [status, setStatus] = useState('');
  const [vin, setVin] = useState('');
  const [customer, setCustomer] = useState('');
  const [technicianId, setTechnicianId] = useState('');
  const [technicians, setTechnicians] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    fetchTechnicians();
  }, []);

  const fetchTechnicians = async () => {
    try {
      const response = await fetch('http://localhost:8080/api/technicians/');
      if (response.ok) {
        const data = await response.json();
        setTechnicians(data.technicians);
      } else {
      }
    } catch (error) {
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const appointmentData = {
      date_time,
      reason,
      status,
      vin,
      customer,
      technician: technicianId,
    };

    try {
      const response = await fetch('http://localhost:8080/api/appointments/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(appointmentData),
      });

      if (response.ok) {
        navigate('/appointments');
      } else {
      }
    } catch (error) {
    }
  };

  return (
    <div>
      <h1>Create Appointment</h1>
      <form onSubmit={handleSubmit}>
        <div>
          <label>Date and Time:</label>
          <input
            type="datetime-local"
            value={date_time}
            onChange={(e) => setDate_time(e.target.value)}
            required
            className="form-control"
          />
        </div>
        <div>
          <label>Reason:</label>
          <input
            type="text"
            value={reason}
            onChange={(e) => setReason(e.target.value)}
            required
            className="form-control"
          />
        </div>
        <div>
          <label>Status:</label>
          <input
            type="text"
            value={status}
            onChange={(e) => setStatus(e.target.value)}
            required
            className="form-control"
          />
        </div>
        <div>
          <label>VIN:</label>
          <input
            type="text"
            value={vin}
            onChange={(e) => setVin(e.target.value)}
            required
            className="form-control"
          />
        </div>
        <div>
          <label>Customer:</label>
          <input
            type="text"
            value={customer}
            onChange={(e) => setCustomer(e.target.value)}
            required
            className="form-control"
          />
        </div>
        <div>
          <label>Technician:</label>
          <select
            value={technicianId}
            onChange={(e) => setTechnicianId(e.target.value)}
            required
            className="form-control"
          >
            <option value="">Select Technician</option>
            {technicians.map((technician) => (
              <option key={technician.id} value={technician.id}>
                {technician.first_name} {technician.last_name}
              </option>
            ))}
          </select>
        </div>
        <button type="submit" className="btn btn-primary">Create</button>
      </form>
    </div>
  );
}

export default AppointmentForm;
