import React from 'react';

function ManufacturersForm(props) {
  return (
    <form onSubmit={props.onSubmit}>
      <div className="form-group">
        <label htmlFor="name">Name:</label>
        <input
          type="text"
          id="name"
          name="name"
          value={props.name}
          onChange={props.onChange}
          className="form-control"
        />
      </div>
      <button type="submit" className="btn btn-primary">
        Add Manufacturer
      </button>
    </form>
  );
}

export default ManufacturersForm;
