function ManufacturersList(props) {
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {props.manufacturers.map(manufacturer => {
            return (
              <tr key={manufacturer.href}>
                <td>{ manufacturer.name }</td>
                <td>{ manufacturer.id }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default ManufacturersList;
