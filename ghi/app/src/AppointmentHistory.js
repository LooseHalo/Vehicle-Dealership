import React, { useState, useEffect } from 'react';

function AppointmentHistory() {
  const [searchVIN, setSearchVIN] = useState('');
  const [appointments, setAppointments] = useState([]);
  const [filteredAppointments, setFilteredAppointments] = useState([]);

  useEffect(() => {
    // Fetch appointments data from the API
    fetch('http://localhost:8080/api/appointments/')
      .then((response) => response.json())
      .then((data) => {
        setAppointments(data.appointments); // Extract appointments array from the response data
      })
      .catch((error) => {
        console.error('Error fetching appointments:', error);
      });
  }, []);

  const handleSearch = (e) => {
    e.preventDefault();

    // Filter appointments by matching VIN
    const filtered = appointments.filter(
      (appointment) => appointment.vin === searchVIN
    );
    setFilteredAppointments(filtered);
  };

  return (
    <div>
      <h1>Appointment History</h1>
      <form onSubmit={handleSearch}>
        <div className="mb-3">
          <label htmlFor="vinInput" className="form-label">
            Search by VIN:
          </label>
          <input
            type="text"
            className="form-control"
            id="vinInput"
            value={searchVIN}
            onChange={(e) => setSearchVIN(e.target.value)}
            placeholder="Enter VIN"
            required
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Search
        </button>
      </form>
      <table className="table table-striped mt-3">
        <thead>
          <tr>
            <th>VIN</th>
            <th>VIP</th>
            <th>Customer</th>
            <th>Date and Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {filteredAppointments.map((appointment) => (
            <tr key={appointment.id}>
              <td>{appointment.vin}</td>
              <td>{appointment.vip ? 'VIP' : 'Regular'}</td>
              <td>{appointment.customer}</td>
              <td>{appointment.date_time}</td>
              <td>{appointment.technician}</td>
              <td>{appointment.reason}</td>
              <td>{appointment.status}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default AppointmentHistory;
