import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

const SaleForm = () => {
  const navigate = useNavigate();
  const [automobiles, setAutomobiles] = useState([]);
  const [salespersons, setSalespersons] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [formData, setFormData] = useState({
    automobile: '',
    salesperson: '',
    customer: '',
    price: '',
  });

  useEffect(() => {
    fetchAutomobiles();
    fetchSalespersons();
    fetchCustomers();
  }, []);

  const fetchAutomobiles = async () => {
    try {
      const response = await fetch('http://localhost:8100/api/automobiles/');
      if (!response.ok) {
        throw new Error();
      }
      const data = await response.json();
      const availableAutomobiles = data && data.autos ? data.autos.filter(
        (automobile) => !automobile.sold
      ) : [];
      setAutomobiles(availableAutomobiles);
    } catch (error) {}
  };

  const fetchSalespersons = async () => {
    try {
      const response = await fetch('http://localhost:8090/api/salespeople/');
      const data = await response.json();
      setSalespersons(data.salespeople);
    } catch (error) {}
  };

  const fetchCustomers = async () => {
    try {
      const response = await fetch('http://localhost:8090/api/customers/');
      const data = await response.json();
      setCustomers(data.customers);
    } catch (error) {}
  };

  const handleFormSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch('http://localhost:8090/api/sales/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          automobile: formData.automobile,
          salesperson: formData.salesperson,
          customer: formData.customer,
          price: formData.price,
        }),
      });

      const data = await response.json();

      const invResponse = await fetch(`http://localhost:8100/api/automobiles/${formData.automobile}/`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          sold: true,
        }),
      });

      const invData = await invResponse.json();

      setFormData({
        automobile: '',
        salesperson: '',
        customer: '',
        price: '',
      });

      navigate('/sales');
    } catch (error) {}
  };

  return (
    <div className="sale-form-container">
      <h1>Record a New Sale</h1>
      <form className="sale-form" onSubmit={handleFormSubmit}>
        <div className="form-group">
          <label>Automobile:</label>
          <select
            className="form-control"
            value={formData.automobile}
            onChange={(e) =>
              setFormData({ ...formData, automobile: e.target.value })
            }
          >
            <option value="">Select an automobile</option>
            {automobiles.map((automobile) => (
              <option key={automobile.vin} value={automobile.vin}>
                {automobile.vin}
              </option>
            ))}
          </select>
        </div>
        <div className="form-group">
          <label>Salesperson:</label>
          <select
            className="form-control"
            value={formData.salesperson}
            onChange={(e) =>
              setFormData({ ...formData, salesperson: e.target.value })
            }
          >
            <option value="">Select a salesperson</option>
            {salespersons.map((salesperson) => (
              <option key={salesperson.id} value={salesperson.employee_id}>
                {salesperson.first_name} {salesperson.last_name}
              </option>
            ))}
          </select>
        </div>
        <div className="form-group">
          <label>Customer:</label>
          <select
            className="form-control"
            value={formData.customer}
            onChange={(e) =>
              setFormData({ ...formData, customer: e.target.value })
            }
          >
            <option value="">Select a customer</option>
            {customers.map((customer) => (
              <option key={customer.id} value={customer.id}>
                {customer.first_name} {customer.last_name}
              </option>
            ))}
          </select>
        </div>
        <div className="form-group">
          <label>Price:</label>
          <input
            className="form-control"
            type="number"
            value={formData.price}
            onChange={(e) =>
              setFormData({ ...formData, price: e.target.value })
            }
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    </div>
  );
};

export default SaleForm;
