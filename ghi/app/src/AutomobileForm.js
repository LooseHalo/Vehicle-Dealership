import React, { useState, useEffect } from 'react';

function AutomobileForm({ onSubmit }) {
  const [vin, setVin] = useState('');
  const [color, setColor] = useState('');
  const [year, setYear] = useState('');
  const [selectedModel, setSelectedModel] = useState('');
  const [models, setModels] = useState([]);

  useEffect(() => {
    async function loadModels() {
      const url = 'http://localhost:8100/api/models/';
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setModels(data.models);
        setSelectedModel(data.models[0]?.id || '');
      }
    }
    loadModels();
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();
    const data = {
      color,
      year: parseInt(year),
      vin,
      model_id: Number(selectedModel),
    };

    onSubmit(data);

    setVin('');
    setColor('');
    setYear('');
    setSelectedModel(models[0]?.id || '');
  };

  const handleVinChange = (event) => {
    setVin(event.target.value);
  };

  const handleColorChange = (event) => {
    setColor(event.target.value);
  };

  const handleYearChange = (event) => {
    setYear(event.target.value);
  };

  const handleModelChange = (event) => {
    setSelectedModel(event.target.value);
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new automobile</h1>
          <form onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input
                onChange={handleVinChange}
                value={vin}
                placeholder="VIN"
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
              />
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleColorChange}
                value={color}
                placeholder="Color"
                required
                type="text"
                name="color"
                id="color"
                className="form-control"
              />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleYearChange}
                value={year}
                placeholder="Year"
                required
                type="number"
                name="year"
                id="year"
                className="form-control"
              />
              <label htmlFor="year">Year</label>
            </div>
            <div className="form-floating mb-3">
              <select
                onChange={handleModelChange}
                value={selectedModel}
                required
                name="model"
                id="model"
                className="form-select"
              >
                <option value="">Choose a model</option>
                {models.map((model) => (
                  <option key={model.id} value={model.id}>
                    {model.name}
                  </option>
                ))}
              </select>
              <label htmlFor="model">Model</label>
            </div>
            <button className="btn btn-primary" type="submit">
              Create
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AutomobileForm;
