import { useNavigate } from 'react-router-dom';
import React, { useState, useEffect } from 'react';

function VehicleModelForm(props) {
    const [name, setName] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [manufacturerId, setManufacturerId] = useState('');
    const [manufacturers, setManufacturers] = useState([]);


    useEffect(() => {
      async function fetchManufacturers() {
        try {
          const response = await fetch('http://localhost:8100/api/manufacturers/');
          if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
          } else {
            console.error(response);
          }
        } catch (error) {
          console.error(error);
        }
      }

      fetchManufacturers();
    }, []);


    const navigate = useNavigate();

    const handleSubmit = async (event) => {
      event.preventDefault();


      try {
        const response = await fetch('http://localhost:8100/api/models/', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            name: name,
            picture_url: pictureUrl,
            manufacturer_id: parseInt(manufacturerId),
          }),
        });

        if (response.ok) {
          navigate('/models');
        } else {
          console.error(response);
        }
      } catch (error) {
        console.error(error);
      }
    };

    return (
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="name">Name:</label>
          <input
            type="text"
            id="name"
            value={name}
            onChange={(e) => setName(e.target.value)}
            required
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label htmlFor="pictureUrl">Picture URL:</label>
          <input
            type="text"
            id="pictureUrl"
            value={pictureUrl}
            onChange={(e) => setPictureUrl(e.target.value)}
            required
            className="form-control"
          />
        </div>
        <div className="form-group">
          <label htmlFor="manufacturerId">Manufacturer:</label>
          <select
            id="manufacturerId"
            value={manufacturerId}
            onChange={(e) => setManufacturerId(e.target.value)}
            required
            className="form-control"
          >
            <option value="">Select Manufacturer</option>
            {manufacturers.map((manufacturer) => (
              <option key={manufacturer.id} value={manufacturer.id}>
                {manufacturer.name}
              </option>
            ))}
          </select>
        </div>
        <button type="submit" className="btn btn-primary">Create Model</button>
      </form>
    );
  }

  export default VehicleModelForm;
