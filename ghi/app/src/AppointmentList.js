import React, { useEffect, useState } from 'react';

function AppointmentList() {
  const [appointments, setAppointments] = useState([]);
  const [vehicles, setVehicles] = useState([]);
  const [technicians, setTechnicians] = useState([]);

  useEffect(() => {
    const fetchAppointments = async () => {
      try {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
          const data = await response.json();
          setAppointments(data.appointments);
        } else {
          console.error(response);
        }
      } catch (error) {
        console.error(error);
      }
    };

    fetchAppointments();
  }, []);

  useEffect(() => {
    const fetchVehicles = async () => {
      try {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
          const data = await response.json();
          setVehicles(data.autos);
        } else {
          console.error(response);
        }
      } catch (error) {
        console.error(error);
      }
    };

    fetchVehicles();
  }, []);

  useEffect(() => {
    const fetchTechnicians = async () => {
      try {
        const response = await fetch('http://localhost:8080/api/technicians/');
        if (response.ok) {
          const data = await response.json();
          setTechnicians(data.technicians);
        } else {
          console.error(response);
        }
      } catch (error) {
        console.error(error);
      }
    };

    fetchTechnicians();
  }, []);

  const isVIPAppointment = (vin) => {
    return vehicles.some((vehicle) => vehicle.vin === vin && vehicle.sold);
  };

  const getTechnicianName = (technicianId) => {
    const technician = technicians.find((tech) => tech.id === technicianId);
    if (technician) {
      return `${technician.first_name} ${technician.last_name}`;
    }
    return 'N/A';
  };

  const handleCancelAppointment = async (appointmentId) => {
    try {
      const url = `http://localhost:8080/api/appointments/${appointmentId}/cancel`;
      const response = await fetch(url, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ status: 'Cancelled' }),
      });
      if (response.ok) {
        const updatedAppointments = appointments.map((appointment) => {
          if (appointment.id === appointmentId) {
            return { ...appointment, status: 'Cancelled' };
          }
          return appointment;
        });
        setAppointments(updatedAppointments);
      } else {
        console.error(response);
      }
    } catch (error) {
      console.error(error);
    }
  };


  const handleFinishAppointment = async (appointmentId) => {
    try {
      const url = `http://localhost:8080/api/appointments/${appointmentId}/finish`;
      const response = await fetch(url, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ status: 'Finished' }),
      });
      if (response.ok) {
        const updatedAppointments = appointments.map((appointment) => {
          if (appointment.id === appointmentId) {
            return { ...appointment, status: 'Finished' };
          }
          return appointment;
        });
        setAppointments(updatedAppointments);
      } else {
        console.error(response);
      }
    } catch (error) {
      console.error(error);
    }
  };



  return (
    <div>
      <h1>Appointments</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Customer</th>
            <th>Vehicle VIN</th>
            <th>Date and Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
            <th>VIP</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map((appointment) => {
            const vehicle = vehicles.find((vehicle) => vehicle.vin === appointment.vin);
            const vehicleVIN = vehicle ? vehicle.vin : appointment.vin;
            const technicianName = getTechnicianName(appointment.technician);
            const vipStatus = isVIPAppointment(appointment.vin) ? 'VIP' : '';
            return (
              <tr key={appointment.id}>
                <td>{appointment.customer}</td>
                <td>{vehicleVIN}</td>
                <td>{appointment.date_time}</td>
                <td>{technicianName}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
                <td>{vipStatus}</td>
                <td>
                  <button onClick={() => handleCancelAppointment(appointment.id)}>Cancel</button>
                  <button onClick={() => handleFinishAppointment(appointment.id)}>Finish</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default AppointmentList;
