import django
import os
import sys
import time
import json
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

from service_rest.models import AutomobileVO


def get_automobiles():
    response = requests.get('http://project-beta-inventory-api-1:8000/api/automobiles/')
    content = json.loads(response.content)

    for automobile in content["autos"]:
        AutomobileVO.objects.update_or_create(
            vin=automobile["vin"],
            defaults={
                "sold": automobile["sold"]
            }
        )


def poll(repeat=True):
    while True:
        try:
            get_automobiles()
            print("get data")
        except Exception as e:
            print(e)
        if not repeat:
            break
        time.sleep(5)
if __name__ == "__main__":
    poll()
