from django.urls import path
from .views import (api_list_automobilevo, api_technician, api_delete_technician, api_list_appointment, api_delete_appointment, api_cancel_appointment, api_finish_appointment)


urlpatterns = [
    path('automobiles/', api_list_automobilevo, name='api_list_automobilevo'),
    path('technicians/', api_technician, name='api_technician'),
    path('appointments/<int:pk>/finish', api_finish_appointment, name='api_finish_appointment'),
    path('appointments/<int:pk>/cancel', api_cancel_appointment, name='api_cancel_appointment'),
    path('technicians/<int:pk>/', api_delete_technician, name='api_delete_technician'),
    path('appointments/<int:pk>/',api_delete_appointment, name='api_delete_appointment'),
    path('appointments/', api_list_appointment, name='api_list_appointment'),
]
