from django.db import models


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=20)
    def __str__(self):
        return f'{self.first_name} {self.last_name} (Employee ID: {self.employee_id})'


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)
    sold = models.BooleanField(default=False)
    def __str__(self):
        return f'VIN: {self.vin}, Sold: {self.sold}'


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=50)
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(Technician, on_delete=models.CASCADE)
    def __str__(self):
        return f'Appointment on {self.date_time} for VIN: {self.vin}'
