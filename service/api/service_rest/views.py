import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Technician, AutomobileVO, Appointment
from django.core.exceptions import ObjectDoesNotExist
from datetime import datetime

class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = ['id', 'first_name', 'last_name', 'employee_id']


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold", "id"]


@require_http_methods(["GET", "POST"])
def api_technician(request):
    if request.method == 'GET':
        technicians = Technician.objects.all()
        technician_list = []
        for technician in technicians:
            technician_list.append({
                'id': technician.id,
                'first_name': technician.first_name,
                'last_name': technician.last_name,
                'employee_id': technician.employee_id
            })
        return JsonResponse({'technicians': technician_list})

    elif request.method == 'POST':
        try:
            data = json.loads(request.body)

            first_name = data.get('first_name')
            last_name = data.get('last_name')
            employee_id = data.get('employee_id')

            technician = Technician.objects.create(
                first_name=first_name,
                last_name=last_name,
                employee_id=employee_id
            )
            return JsonResponse({'technician': {
                'id': technician.id,
                'first_name': technician.first_name,
                'last_name': technician.last_name,
                'employee_id': technician.employee_id
            }})

        except Exception as e:
            return JsonResponse({'message': 'Could not create the Technician'}, status=400)

    else:
        return JsonResponse({'message': 'Invalid request method'}, status=405)


@require_http_methods(['GET'])
def api_list_automobilevo(request):
    automobiles = AutomobileVO.objects.all()
    return JsonResponse(
        {'automobiles': list(automobiles.values())},
        encoder=AutomobileVODetailEncoder,
    )


@require_http_methods(["GET", "POST"])
def api_list_appointment(request, pk=None):
    if request.method == 'GET':
        if pk:
            try:
                appointment = Appointment.objects.get(id=pk)
                appointment_data = {
                    'id': appointment.id,
                    'date_time': appointment.date_time.isoformat(),
                    'reason': appointment.reason,
                    'status': appointment.status,
                    'vin': appointment.vin,
                    'customer': appointment.customer,
                    'technician': appointment.technician.id
                }
                return JsonResponse({'appointment': appointment_data})
            except Appointment.DoesNotExist:
                return JsonResponse({'message': 'Appointment not found'}, status=404)
        else:
            appointments = Appointment.objects.all()
            appointment_list = []
            for appointment in appointments:
                appointment_data = {
                    'id': appointment.id,
                    'date_time': appointment.date_time.isoformat(),
                    'reason': appointment.reason,
                    'status': appointment.status,
                    'vin': appointment.vin,
                    'customer': appointment.customer,
                    'technician': appointment.technician.id
                }
                appointment_list.append(appointment_data)
            return JsonResponse({'appointments': appointment_list})

    elif request.method == 'POST':
        try:

            data = json.loads(request.body)


            date_time = data.get('date_time')
            reason = data.get('reason')
            status = data.get('status')
            vin = data.get('vin')
            customer = data.get('customer')
            technician_id = data.get('technician')


            if not date_time or not reason or not status or not vin or not customer or not technician_id:
                return JsonResponse({'message': 'Invalid request. Please provide all the required fields.'}, status=400)


            date_time = datetime.fromisoformat(date_time)


            technician = Technician.objects.get(id=technician_id)


            appointment = Appointment.objects.create(
                date_time=date_time,
                reason=reason,
                status=status,
                vin=vin,
                customer=customer,
                technician=technician
            )

            # Return the response with the created appointment details
            return JsonResponse({'appointment': {
                'id': appointment.id,
                'date_time': appointment.date_time.isoformat(),
                'reason': appointment.reason,
                'status': appointment.status,
                'vin': appointment.vin,
                'customer': appointment.customer,
                'technician': {
                    'id': appointment.technician.id,
                    'first_name': appointment.technician.first_name,
                    'last_name': appointment.technician.last_name,
                    'employee_id': appointment.technician.employee_id
                }
            }})

        except Exception as e:
            return JsonResponse({'message': f'Error creating the appointment: {str(e)}'}, status=400)

        except Technician.DoesNotExist:
            return JsonResponse({'message': 'Technician not found'}, status=400)

        except Exception as e:
            return JsonResponse({'message': f'Error creating the appointment: {str(e)}'}, status=500)

    else:
        return JsonResponse({'message': 'Invalid request method'}, status=405)


@require_http_methods(["POST"])
def api_create_appointment(request):
    try:
        data = json.loads(request.body)
        date_time = data.get('date_time')
        reason = data.get('reason')
        status = data.get('status')
        vin = data.get('vin')
        customer = data.get('customer')
        technician_id = data.get('technician')

        if not date_time or not reason or not status or not vin or not customer or not technician_id:
            return JsonResponse({'message': 'Invalid request. Please provide all the required fields.'}, status=400)

        date_time = datetime.datetime.fromisoformat(date_time)

        technician = Technician.objects.get(id=technician_id)

        appointment = Appointment.objects.create(
            date_time=date_time,
            reason=reason,
            status=status,
            vin=vin,
            customer=customer,
            technician=technician
        )

        return JsonResponse({'appointment': {
            'id': appointment.id,
            'date_time': appointment.date_time.isoformat(),
            'reason': appointment.reason,
            'status': appointment.status,
            'vin': appointment.vin,
            'customer': appointment.customer,
            'technician': {
                'id': appointment.technician.id,
                'first_name': appointment.technician.first_name,
                'last_name': appointment.technician.last_name,
                'employee_id': appointment.technician.employee_id
            }
        }})

    except Exception as e:
        return JsonResponse({'message': f'Error creating the appointment: {str(e)}'}, status=400)

@require_http_methods(["DELETE"])
def api_delete_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        appointment.delete()
        return JsonResponse({'message': 'Appointment deleted successfully'})

    except Appointment.DoesNotExist:
        return JsonResponse({'message': 'Appointment not found'}, status=404)

@require_http_methods(["DELETE"])
def api_delete_technician(request, pk):
    try:
        technician = Technician.objects.get(id=pk)
        technician.delete()
        return JsonResponse({'message': 'Technician deleted successfully'})

    except Technician.DoesNotExist:
        return JsonResponse({'message': 'Technician not found'}, status=404)

@require_http_methods(["PUT"])
def api_finish_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        appointment.status = "Finished"
        appointment.save()
        return JsonResponse({'message': 'Appointment Finished'})

    except Appointment.DoesNotExist:
        return JsonResponse({'message': 'Appointment not found'}, status=404)


@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        appointment.status = "Cancelled"
        appointment.save()
        return JsonResponse({'message': 'Appointment Cancelled'})

    except Appointment.DoesNotExist:
        return JsonResponse({'message': 'Appointment not found'}, status=404)
