# Vehicle Dealership Toolbox

## Our Team :

* Rene Hinojosa: Service :
* Maria Arutyunova: Sales :


## How to Run this App
 - First you will need to go to this link: https://github.com/LooseHalo/Vehicle-Dealership
 - Second you will need to fork and clone
 - Once you have Docker (or your preferred manager) running and are in the proper directory in your terminal you will need to run these commands in this order:

1. docker volume create beta-data
2. docker-compose build
3. docker-compose up

This will get you all set up to run the application.

## Diagram
 - Here is a diagram of how our project works together.
 ![img](/images/DiagramForFinalProject.png)

## Design

For our project we have three microservices that all depend on eachother. Our project is called Vehicle Dealership Toolbox and we have our inventory, sale and service microservices. Our inventory side handles the manafacturers, automobiles and models. We need this service because our sales and services depends on the automobiles, manafacturers and models to interact with our models. Below we will describe a couple of ways that this happens.

## Team Work Section!

There are a few ways where Rene and Maria worked as a team! One of the ways was getting the inventory front end done on the first day of our project! Rene handled the front end for Models and Maria handled the front end for Manafacturers and Automobiles. We worked together when trying to resolve merge conflicts. We had to work together when we had to do our App.js file and our Nav.js file. We also tried our best to help eachother during the project if either of us had questions about service or sales.


# API Documentation. This includes Ports and URLS for Inventory, Service and Sales. (CRUD)


# Inventory Api LocalHost 8100 information:
-**To Interact with the Manafactur Model:**

Action | Method | URL |
|---|-------|-------------------|
List Manufacturers | GET | http://localhost:8100/api/manufacturers/
Create a manufacturer|POST|http://localhost:8100/api/manufacturers/
Get a specific manufacturer|GET|http://localhost:8100/api/manufacturers/:id/|
Update a specific manufacturer|	PUT|	http://localhost:8100/api/manufacturers/:id/
Delete a specific manufacturer|	DELETE|	http://localhost:8100/api/manufacturers/:id/

------API Legend Information--------
* Creating and updating a manufacturer requires only the manufacturer's name.


```
{
  "name": "Chrysler"
}
```

* The return value of creating, getting, and updating a single manufacturer is its name, href, and id.

```
{
  {
  "href": "/api/manufacturers/1/",
  "id": 1,
  "name": "Chrysler"
}
```
* The list of manufacturers is a dictionary with the key "manufacturers" set to a list of manufacturers.

```
{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  ]
}
```
-**To Interact with the Vehicle Model:**

Action | Method | URL |
|---|-------|-------------------|
List vehicle models|GET|http://localhost:8100/api/models/|
|Create a vehicle model|POST|http://localhost:8100/api/models/|
|Get a specific vehicle model|GET|http://localhost:8100/api/models/:id/|
Update a specific vehicle model|PUT|http://localhost:8100/api/models/:id/|
Delete a specific vehicle model|DELETE|	http://localhost:8100/api/models/:id/|

------API Legend Information--------
* Creating a vehicle model requires the model name, a URL of an image, and the id of the manufacturer.

```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}
```
* Updating a vehicle model can take the name and/or the picture URL. It is not possible to update a vehicle model's manufacturer.

```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
}

```
* Getting the detail of a vehicle model, or the return value from creating or updating a vehicle model, returns the model's information and the manufacturer's information.


```
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Daimler-Chrysler"
  }
}
```
* Getting a list of vehicle models returns a list of the detail information with the key "models".

```
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }
  ]
}
```
-**To Interact with the Automobile Model**

Action | Method | URL |
|---|-------|-------------------|
List automobiles|GET|http://localhost:8100/api/automobiles/|
Create an automobile|POST|http://localhost:8100/api/automobiles/|
Get a specific automobile|GET|http://localhost:8100/api/automobiles/:vin/|
Update a specific automobile|PUT|http://localhost:8100/api/automobiles/:vin/|
Delete a specific automobile|DELETE|http://localhost:8100/api/automobiles/:vin/|

------API Legend Information--------
* You can create an automobile with its color, year, VIN, and the id of the vehicle model.

```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}
```
* As noted, you query an automobile by its VIN. For example, you would use the URL

http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/

to get the details for the car with the VIN "1C3CC5FB2AN120174". The details for an automobile include its model and manufacturer.

```
{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "yellow",
  "year": 2013,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  },
  "sold": false
}
```
* You can update the color, year, and sold status of an automobile.

```
{
  "color": "red",
  "year": 2012,
  "sold": true
}
```
* Getting a list of automobiles returns a dictionary with the key "autos" set to a list of automobile information.

```
{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      },
      "sold": false
    }
  ]
}

```

# Service API Localhost 8080 information

-**To interact with the Technician Model**
Action | Method | URL |
|---|-------|-------------------|
List  technicians|GET|http://localhost:8080/api/technicians/|
Create a technician|POST|http://localhost:8080/api/technicians/|
Delete a specific technician|DELETE|http://localhost:8080/api/technicians/:id|


------API Legend Information--------

* requesting a GET List Technicians will use no body, and return a response similar to this;
```
{
	"technicians": [
		{
			"id": 1,
			"first_name": "John",
			"last_name": "Wayne",
			"employee_id": "12664"
		},
		{
			"id": 2,
			"first_name": "John",
			"last_name": "Wayne",
			"employee_id": "164582"
		},
		{
			"id": 3,
			"first_name": "John",
			"last_name": "Wayne",
			"employee_id": "44585"
		}
	]
}
```

* Creating a Technician will have a POST request body similar to;

```
{
  "first_name": "John",
  "last_name": "Wayne",
  "employee_id": 44585
}
```

* Deleting a Technician will use the ID of the technician in the URL with no body, and generate the response below;

```
{
	"message": "Technician deleted successfully"
}
```



-**To interact with the Appointments Model**
Action | Method | URL |
|---|-------|-------------------|
List appointments|GET|http://localhost:8080/api/appointments/|
Create an appointment|POST|http://localhost:8080/api/appointments/|
Delete an appointment|DELETE|http://localhost:8080/api/appointments/:id|
Set appointment status to "canceled"|PUT|http://localhost:8080/api/appointments/:id/cancel|
Set appointment status to "finished"|PUT|http://localhost:8080/api/appointments/:id/finish|

------API Legend Information--------

* List Appointments GET request will use no body, and have a response similar to this;
```
{
	"appointments": [
		{
			"id": 8,
			"date_time": "2023-06-15T10:30:00+00:00",
			"reason": "Routine check-up",
			"status": "Scheduled",
			"vin": "1C3CC5FB2AN120174",
			"customer": "John Doe",
			"technician": 1
		}
	]
}
```

Create an Appointment POST request will have a body structure as below;
```
{
  "date_time": "2023-06-15T10:30:00",
  "reason": "Routine check-up",
  "status": "Scheduled",
  "vin": "123TEST",
  "customer": "John Doe",
  "technician": 1
}
```
* Delete an Appointment DELETE request will use no body, and have the following successful response when completed;
```
{
	"message": "Appointment Deleted"
}
```
*Set Appointment Status to "Cancelled" PUT request will use the ID in teh request url, and provide a success response when completed;
```
{
	"message": "Appointment Cancelled"
}
```
*Set Appointment Status to "Finished" PUT request will use the ID in teh request url, and provide a success response when completed;
```
{
	"message": "Appointment Finished"
}
```


-**To interact with the AutomobileVO Model**

Action | Method | URL |
|---|-------|-------------------|
List AutomobileVO|GET|http://localhost:8080/api/automobiles/|


------API Legend Information--------

* The request for AutomobileVO list has no body, and returns automobiles in a list;
```
{
	"automobiles": [
		{
			"id": 1,
			"vin": "1C3CC5FB2AN120174",
			"sold": true
		},
		{
			"id": 2,
			"vin": "1C3CC4685HG120174",
			"sold": false
		},
		{
			"id": 3,
			"vin": "1C3CC4685HG122248",
			"sold": false
		}
	]
}
```
# Sales API LocalHost 8090 information


-**To Interact with the SalesPerson Model:**


Action | Method | URL |
|---|-------|-------------------|
List salespeople|GET|http://localhost:8090/api/salespeople/|
Create a salesperson|POST|http://localhost:8090/api/salespeople/|
Delete a specific salesperson|DELETE|http://localhost:8090/api/salespeople/:id|

------API Legend Information--------

* The list of Salespeople will return the id, first name, last name and employee id:
```
{
	"salespeople": [
		{
			"id": 1,
			"first_name": "Blake",
			"last_name": "B",
			"employee_id": "BB123"
		}
	]
}
```

* To create a salesperson your body should look like this and include the first name, last name and employee id in the JSON body:
```
{
  "first_name": "Blake",
  "last_name": "B",
  "employee_id": "BB123"
}
```

* The delete saleperson will return True after entering the salespeople ID at the end of the URL

```
{
	"deleted": true
}
```
-**To Interact with the Customer Model:**

Action | Method | URL |
|---|-------|-------------------|
List customers|GET|http://localhost:8090/api/customers/|
Create a customer|POST|http://localhost:8090/api/customers/|
Delete a specific customer|DELETE|http://localhost:8090/api/customers/:id|


------API Legend Information--------


* Listing customers will show you the ID, first name, last name, address and phone number like this:

```
{
	"customers": [
		{
			"id": 1,
			"first_name": "Rene",
			"last_name": "R",
			"address": "123 My Street, State, USA",
			"phone_number": "111-111-1111"
		}
	]
}
```
* To create a customer you need the first name, last name, address and phone number. Your JSON body should look like this:

```
{
	"first_name": "Rene",
	"last_name": "R",
	"address": "123 My Street, State, USA",
	"phone_number": "111-111-1111",
	"id": 1
}
```

* If you need to delete a customer, once a customer is deleted it will show True like this:

```
{
	"deleted": true
}
```

-**To Interact with the Sales Model:**
Action | Method | URL |
|---|-------|-------------------|
List sales|GET|http://localhost:8090/api/sales/|
Create a sale|POST|http://localhost:8090/api/sales/|
Delete a sale|DELETE|http://localhost:8090/api/sales/:id|
To Show Sales History|GET|http://localhost:8090/api/salespeople/BB123/sales|
To show an individual sale|GET|http://localhost:8090/api/sales/id/|

* The sales list will show you the automobile, salesperson,customer and price with their included attributes like this:

```
{
	"sales": [
		{
			"automobile": {
				"vin": "767678",
				"sold": true,
				"id": 1
			},
			"salesperson": {
				"first_name": "Blake",
				"last_name": "B",
				"employee_id": "BB123",
				"id": 1
			},
			"customer": {
				"first_name": "Rene",
				"last_name": "R",
				"address": "123 My Street, State, USA",
				"phone_number": "111-111-1111",
				"id": 1
			},
			"price": 40000,
			"id": 3
		}
	]
}
```

* To create a sale you will need automobile (VIN), salesperson(employee ID), customer (id), and price which will look similiar to this:

```
{
    "automobile": "767678",
    "salesperson": "BB123",
    "customer": 1,
    "price": 40000
}

```
* Deleting a sale you will need the sale ID in the URL and you will get deleted = True like this:

```
{
	"deleted": true
}
```

* You can also show an individual sale with the sale/id in a GET request for the URL and you will get the automobile, saleperson, customer and price similiar to this:

```
{
	"automobile": {
		"vin": "767678",
		"sold": true,
		"id": 1
	},
	"salesperson": {
		"first_name": "Blake",
		"last_name": "B",
		"employee_id": "BB123",
		"id": 1
	},
	"customer": {
		"first_name": "Rene",
		"last_name": "R",
		"address": "123 My Street, State, USA",
		"phone_number": "111-111-1111",
		"id": 1
	},
	"price": 40000,
	"id": 3
}

```

* For my front-end website to show a history of sales by a certain salesperson in the dropdown menu I needed to add an extra view for saleshistory. To do this you will need to include the employee's ID in the URL and this will return something similiar to this:

```
{
	"sales": [
		{
			"automobile": {
				"vin": "767678",
				"sold": true,
				"id": 1
			},
			"salesperson": {
				"first_name": "Blake",
				"last_name": "B",
				"employee_id": "BB123",
				"id": 1
			},
			"customer": {
				"first_name": "Rene",
				"last_name": "R",
				"address": "123 My Street, State, USA",
				"phone_number": "111-111-1111",
				"id": 1
			},
			"price": 40000,
			"id": 3
		},
		{
			"automobile": {
				"vin": "12345678",
				"sold": true,
				"id": 2
			},
			"salesperson": {
				"first_name": "Blake",
				"last_name": "B",
				"employee_id": "BB123",
				"id": 1
			},
			"customer": {
				"first_name": "Rene",
				"last_name": "R",
				"address": "123 My Street, State, USA",
				"phone_number": "111-111-1111",
				"id": 1
			},
			"price": 50000,
			"id": 4
		},
		{
			"automobile": {
				"vin": "55555555555",
				"sold": true,
				"id": 3
			},
			"salesperson": {
				"first_name": "Blake",
				"last_name": "B",
				"employee_id": "BB123",
				"id": 1
			},
			"customer": {
				"first_name": "Rene",
				"last_name": "R",
				"address": "123 My Street, State, USA",
				"phone_number": "111-111-1111",
				"id": 1
			},
			"price": 800000,
			"id": 5
		}
	]
}
```

-**To Interact with the AutomobileVO Model:**
Action | Method | URL |
|---|-------|-------------------|
List AutomobileVo|GET|http://localhost:8090/api/automobiles/|


* If you need to check if your poller is working you can actually check your automobileVO it will show you the cars from automobiles inventory and if they have been sold or not

```
{
	"automobiles": [
		{
			"id": 1,
			"vin": "767678",
			"sold": true
		},
		{
			"id": 2,
			"vin": "12345678",
			"sold": true
		}
	]
}
```


## Service microservice Rene

The Service microservice is setup to allow a consierge to record appointments noting date/time, vehicle VIN, reson, customer name and assigned technician and records this data with relation to the Technician assigned. The VIN given is checked against the inventory records and noted as VIP if this VIN is one that was proveded by this establishment, within the appointments page. This page lists the appointments with a current status and buttons for easily marking an appointment as Cancelled or Finished. The list of Technicians is also available at the technicians page, showing all Technicians on staff. The technicians/create page allows for the addition of a new Technician with the needed information and is available to be assigned to any new appointments.

The technician and VehicleVO models are used with the Appointments, Technicians are related as a foreign object, while the VehicleVO is used to check the provided VIN in the appointment agains the inventory record of VINs for a VIP status.

The service history page shows the history of service appointments for a specific VIN when provided.



## Sales microservice Maria


My models in the sales microservice were the Salesperson, Customer, Sale and AutomobileVO. I integrated the sales service with the inventory service through the automobileVO specifically with the VIN and sold status. Without the VIN I was not able to create sales so I need the VIN to be able to create a sale. I also needed all of my other models like the Salesperson and customer to also create the sale. That is why my models are all so important because the Sales model depends on the other models functioning perfectly to work itself.

I wanted to make a note that my special feature that only shows un-sold vehicles in the dropdown menu is implemented in my front-end. When I checked it, it will also eventually update in the backend and mark a vehicle as sold too. It should update my inventory AutomobileList page but it does take a second to update there. I think that has something to do with my poller. So if it doesn't update right away it will because I link my poller and my inventory in the backend and also in the front-end.

I also had to add an additional view for saleshistory so that it would work with my Salesperson History dropdown menu.


These are some of the ways that my models have been integrated with the inventory api.

​
​
## Value Objects
The only Value Object that we had was our AutomobileVO which we both used in service and sales. In sales we used the AutomobileVO for the poller to pull information from automobiles in inventory for tracking sales with the VIN and Sold status. For service we used the automobileVO for appointments and we used it specifically to link the VIN number to the appointment.
