from django.urls import path
from .api_views import (
    api_list_salespeople,
    api_show_salesperson,
    api_list_customers,
    api_show_customer,
    api_list_sales,
    api_show_sale,
    api_list_automobilevo,
    api_salesperson_sales,
)

urlpatterns = [
    path('salespeople/', api_list_salespeople, name='api_list_salespeople'),
    path("salespeople/<int:pk>/", api_show_salesperson, name="api_show_salesperson"),
    path("salespeople/<str:employee_id>/sales", api_salesperson_sales, name="api_salesperson_sales"),
    path('customers/', api_list_customers, name='api_list_customers'),
    path("customers/<int:pk>/", api_show_customer, name="api_show_customer"),
    path('sales/', api_list_sales, name='api_list_sales'),
    path('sales/<int:pk>/', api_show_sale, name='api_show_sale'),
    path('automobiles/', api_list_automobilevo, name='api_list_automobilevo'),
]
