import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Salesperson, Customer, AutomobileVO, Sale
from django.core.exceptions import ObjectDoesNotExist
from django.core.exceptions import FieldError
from django.db import transaction
from django.shortcuts import get_object_or_404



class SalespersonEncoder(ModelEncoder):
    def default(self, obj):
        if isinstance(obj, Salesperson):
            return {
                "first_name": obj.first_name,
                "last_name": obj.last_name,
                "employee_id": obj.employee_id
            }
        return super().default(obj)


class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "address", "phone_number", "id"]


class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "address", "phone_number", "id"]


class SalespersonDetailEncoder(ModelEncoder):
    model = Salesperson
    properties = ["first_name", "last_name", "employee_id", "id"]


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold", "id"]

class SaleListEncoder(ModelEncoder):
    model = Sale
    properties = ["automobile", "salesperson", "customer", "price", "id"]
    encoders = {
        "automobile": AutomobileVODetailEncoder(),
        "salesperson": SalespersonDetailEncoder(),
        "customer": CustomerDetailEncoder()
    }

    def default(self, obj):
        if isinstance(obj, Sale):
            return {
                "automobile": AutomobileVODetailEncoder().default(obj.automobile),
                "salesperson": SalespersonDetailEncoder().default(obj.salesperson),
                "customer": CustomerDetailEncoder().default(obj.customer),
                "price": obj.price,
                "id": obj.id
            }
        return super().default(obj)


class SaleDetailEncoder(ModelEncoder):
    model = Sale
    properties = ["automobile", "salesperson", "customer", "price", "id"]
    encoders = {
        "automobile": AutomobileVODetailEncoder(),
        "salesperson": SalespersonDetailEncoder(),
        "customer": CustomerDetailEncoder()
    }


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": list(salespeople.values())},
            safe=False
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_salesperson(request, pk):
    try:
        salesperson = Salesperson.objects.get(id=pk)
    except Salesperson.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid salesperson id"},
            status=400,
        )

    if request.method == "GET":
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        salesperson.delete()
        return JsonResponse({"deleted": True})
    elif request.method == "PUT":
        content = json.loads(request.body)
        for key, value in content.items():
            if hasattr(salesperson, key):
                setattr(salesperson, key, value)
        salesperson.save()
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": list(customers.values())},
            encoder=CustomerListEncoder
        )
    else:  # POST
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_customer(request, pk):
    try:
        customer = Customer.objects.get(id=pk)
    except Customer.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid customer id"},
            status=400,
        )

    if request.method == "GET":
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        customer.delete()
        return JsonResponse({"deleted": True})
    elif request.method == "PUT":
        content = json.loads(request.body)
        for key, value in content.items():
            if hasattr(customer, key):
                setattr(customer, key, value)
        customer.save()
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        sales_list = [SaleListEncoder().default(sale) for sale in sales]
        return JsonResponse({"sales": sales_list}, safe=False)

    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            automobile_vin = content["automobile"]
            salesperson_id = content["salesperson"]
            customer_id = content["customer"]
            price = content["price"]
        except KeyError as e:
            return JsonResponse(
                {"message": f"Key {str(e)} not found in request body"},
                status=400,
            )

        try:
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            if automobile.sold:
                return JsonResponse(
                    {"message": "This car has already been sold"},
                    status=400,
                )
            salesperson = Salesperson.objects.get(employee_id=salesperson_id)
            customer = Customer.objects.get(id=customer_id)
        except ObjectDoesNotExist as e:
            return JsonResponse(
                {"message": f"{str(e)} does not exist in database"},
                status=400,
            )

        try:
            with transaction.atomic():
                sale = Sale.objects.create(
                    automobile=automobile,
                    salesperson=salesperson,
                    customer=customer,
                    price=price
                )
                automobile.sold = True
                automobile.save()
        except Exception as e:
            return JsonResponse(
                {"message": f"Error while creating Sale object: {str(e)}"},
                status=400,
            )

        sale_dict = SaleDetailEncoder().default(sale)
        return JsonResponse(sale_dict, encoder=SaleDetailEncoder, safe=False)



@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_sale(request, pk):
    try:
        sale = Sale.objects.get(id=pk)
    except Sale.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid sale id"},
            status=400,
        )

    if request.method == "GET":
        return JsonResponse(
            sale,
            encoder=SaleDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        sale.delete()
        return JsonResponse({"deleted": True})
    elif request.method == "PUT":
        content = json.loads(request.body)
        for key, value in content.items():
            if hasattr(sale, key):
                try:
                    setattr(sale, key, value)
                except FieldError as e:
                    return JsonResponse(
                        {"message": f"Error in field assignment: {str(e)}"},
                        status=400,
                    )
        sale.save()
        return JsonResponse(
            sale,
            encoder=SaleDetailEncoder,
            safe=False,
        )



@require_http_methods(['GET'])
def api_list_automobilevo(request):
    automobiles = AutomobileVO.objects.all()
    return JsonResponse(
        {'automobiles': list(automobiles.values())},
        encoder=AutomobileVODetailEncoder,
    )

@require_http_methods(["GET"])
def api_salesperson_sales(request, employee_id):
    salesperson = get_object_or_404(Salesperson, employee_id=employee_id)
    sales = Sale.objects.filter(salesperson=salesperson)
    sales_list = [SaleListEncoder().default(sale) for sale in sales]
    return JsonResponse({"sales": sales_list}, safe=False)
