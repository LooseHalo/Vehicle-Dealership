import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

from sales_rest.models import AutomobileVO


def get_automobiles():
    response = requests.get('http://project-beta-inventory-api-1:8000/api/automobiles/')
    content = json.loads(response.content)

    for automobile in content["autos"]:
        vin = automobile["vin"]
        sold = automobile["sold"]

        try:
            automobile_vo = AutomobileVO.objects.get(vin=vin)
            if automobile_vo.sold and not sold:
                continue
        except AutomobileVO.DoesNotExist:
            pass

        AutomobileVO.objects.update_or_create(
            vin=vin,
            defaults={"sold": sold}
        )


def poll(repeat=True):
    while True:
        try:
            get_automobiles()
            print("Updated automobile VO data")
        except Exception as e:
            print(e)
        if not repeat:
            break
        time.sleep(1)


if __name__ == "__main__":
    poll()
